#!groovy
node {
	def mvnHome = tool 'MAVEN3.3.9'

	stage('Checkout') { 
		checkout scm
	}

	stage('Build') {
		if (isUnix()) {
			sh "'${mvnHome}/bin/mvn' install -DskipTests"
		} else {
			bat(/"${mvnHome}\bin\mvn" install -DskipTests/)
		}
	}

	stage('Test') {
		if (isUnix()) {
			sh "'${mvnHome}/bin/mvn' test"
		} else {
			bat(/"${mvnHome}\bin\mvn" test/)
		}
		junit '**/target/surefire-reports/TEST-*.xml'
	}

	stage('Code quality') {
		if (isUnix()) {
			sh "'${mvnHome}/bin/mvn' sonar:sonar"
		} else {
			def sonarUrl = 'http://localhost:9005'
			bat(/"${mvnHome}\bin\mvn" sonar:sonar -Dsonar.host.url="${sonarUrl}"/)
		}
	}

	stage('Archive') {
		archive 'target/*.jar'
	}
}
